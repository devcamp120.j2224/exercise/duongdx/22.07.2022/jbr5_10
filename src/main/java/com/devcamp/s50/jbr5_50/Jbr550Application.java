package com.devcamp.s50.jbr5_50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr550Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr550Application.class, args);
	}

}
