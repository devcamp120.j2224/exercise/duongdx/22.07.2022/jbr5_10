package com.devcamp.s50.jbr5_50.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr5_50.model.Region;

@Service
public class RegionService {
    Region HaNoi = new Region("HN", "Ha Noi");
    Region HaiDuong = new Region("HD", "Hai Duong");
    Region Tokyo = new Region("TO", "Tokyo");
    Region ibaraki = new Region("IB", "ibaraki");
    Region Ashima = new Region("AS", "Ashima");
    Region Brazil = new Region("BZ", "Brazil");
    
    public ArrayList<Region> getRegionVn(){
        ArrayList<Region> regionsVN = new ArrayList<Region>();

       regionsVN.add(HaNoi);
       regionsVN.add(HaiDuong);
       
        return regionsVN;
    }
    public ArrayList<Region> getRegionJP(){
        ArrayList<Region> regionsJP = new ArrayList<Region>();

        regionsJP.add(Tokyo);
        regionsJP.add(ibaraki);

        return regionsJP;
    }
    public ArrayList<Region> getRegionBZ(){
        ArrayList<Region> regionsBz = new ArrayList<Region>();

        regionsBz.add(Ashima);
        regionsBz.add(Brazil);

        return regionsBz;
    }

    public ArrayList<Region> getRegionsAll() {
        ArrayList<Region> regionsAll = new ArrayList<Region>();
        regionsAll.add(HaNoi);
        regionsAll.add(HaiDuong);
        regionsAll.add(Tokyo);
        regionsAll.add(ibaraki);
        regionsAll.add(Ashima);
        regionsAll.add(Brazil);

        return regionsAll;
        
    }
}
