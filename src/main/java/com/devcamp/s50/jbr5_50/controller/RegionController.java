package com.devcamp.s50.jbr5_50.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.jbr5_50.model.Region;
import com.devcamp.s50.jbr5_50.service.RegionService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/regions-info")
    public Region getRegionInfo(@RequestParam("regionCode") String regionCode) {
            ArrayList<Region> regions = regionService.getRegionsAll();
        
            Region regionFind = new Region();
            for(Region region : regions) {
                if(region.getRegionCode().equals(regionCode)) {
                    regionFind = region;
                }
            }
            return regionFind;
    }
}
