package com.devcamp.s50.jbr5_50.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr5_50.model.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionservice;
        Country VietNam = new Country("VietNam", "VN");
        Country japan = new Country("Japan", "JP");
        Country China = new Country("China", "CN");
    
    public ArrayList<Country> getCountryList() {
        ArrayList<Country> countryList = new ArrayList<>();
        
        VietNam.setRegions(regionservice.getRegionVn());
        japan.setRegions(regionservice.getRegionJP());
        China.setRegions(regionservice.getRegionBZ());

        countryList.add(VietNam);
        countryList.add(japan);
        countryList.add(China);

        return countryList;
    }
}
