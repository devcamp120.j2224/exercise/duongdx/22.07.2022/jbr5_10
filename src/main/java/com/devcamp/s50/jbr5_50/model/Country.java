package com.devcamp.s50.jbr5_50.model;

import java.util.ArrayList;

public class Country {
    private String countryName;
    private String countryCode;
    private ArrayList<Region> regions = new ArrayList<>();
    
    public Country(String countryName, String countryCode, ArrayList<Region> regions) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.regions = regions;
    }

    public Country() {
    }

    public Country(String countryName, String countryCode) {
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

  

    
}
