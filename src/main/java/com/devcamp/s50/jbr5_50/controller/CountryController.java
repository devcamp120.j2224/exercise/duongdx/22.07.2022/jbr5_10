package com.devcamp.s50.jbr5_50.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.jbr5_50.model.Country;
import com.devcamp.s50.jbr5_50.service.CountryService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public Country getCountryInfo(@RequestParam("countryId") String countryId){
        ArrayList<Country> countries = countryService.getCountryList();

        Country findCountry = new Country();
        for(Country country : countries){
            if(country.getCountryCode().equals(countryId)){
                findCountry = country ;
            }
        }
        return findCountry;
    }

    @GetMapping("/country")
    public ArrayList<Country> getCountry(){
        ArrayList<Country> allCountry = countryService.getCountryList();
        return allCountry;
    }
}
